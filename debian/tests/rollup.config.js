import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';

export default {
	input: 'debian/tests/test.js',

	plugins: [
		resolve({
			customResolveOptions: {
			        moduleDirectory: ['/usr/share/nodejs', '/usr/lib/nodejs']
			      }
		}),
		commonjs()
	],

	output: {
			format: 'iife',
			file: 'debian/tests/bundle.js',
			name: 'MyApp'
		},
};
